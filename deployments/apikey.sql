
CREATE TABLE `ApiKey` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Client_id` varchar(200) DEFAULT NULL,
  `Client_secret` varchar(200) DEFAULT NULL,
  `Application` varchar(200) NOT NULL ,
  `ApplicationVersion` varchar(200) NOT NULL ,
  `Company` varchar(1024) NOT NULL ,
  `DateCreated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
);

CREATE UNIQUE INDEX Application ON ApiKey(Application,ApplicationVersion);
