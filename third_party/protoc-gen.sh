protoc --proto_path=api/proto/1.0.0 --proto_path=third_party --go_out=plugins=grpc:pkg/api/1.0.0 apikey-service.proto
protoc --proto_path=api/proto/1.0.0 --proto_path=third_party --grpc-gateway_out=logtostderr=true:pkg/api/1.0.0 apikey-service.proto
protoc --proto_path=api/proto/1.0.0 --proto_path=third_party --swagger_out=logtostderr=true:api/swagger/1.0.0 apikey-service.proto
