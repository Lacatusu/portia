// Code generated by protoc-gen-go. DO NOT EDIT.
// source: apikey-service.proto

package v1

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	_ "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// ApiKey which provide to the application clients.
type ApiKey struct {
	// Unique integer identifier of the apiKey
	Id int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	//
	ClientId string `protobuf:"bytes,2,opt,name=client_id,json=clientId,proto3" json:"client_id,omitempty"`
	//
	ClientSecret string `protobuf:"bytes,3,opt,name=client_secret,json=clientSecret,proto3" json:"client_secret,omitempty"`
	//
	Application string `protobuf:"bytes,4,opt,name=application,proto3" json:"application,omitempty"`
	//
	ApplicationVersion string `protobuf:"bytes,5,opt,name=applicationVersion,proto3" json:"applicationVersion,omitempty"`
	Company            string `protobuf:"bytes,6,opt,name=company,proto3" json:"company,omitempty"`
	//
	DateCreated          *timestamp.Timestamp `protobuf:"bytes,7,opt,name=dateCreated,proto3" json:"dateCreated,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *ApiKey) Reset()         { *m = ApiKey{} }
func (m *ApiKey) String() string { return proto.CompactTextString(m) }
func (*ApiKey) ProtoMessage()    {}
func (*ApiKey) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{0}
}

func (m *ApiKey) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ApiKey.Unmarshal(m, b)
}
func (m *ApiKey) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ApiKey.Marshal(b, m, deterministic)
}
func (m *ApiKey) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ApiKey.Merge(m, src)
}
func (m *ApiKey) XXX_Size() int {
	return xxx_messageInfo_ApiKey.Size(m)
}
func (m *ApiKey) XXX_DiscardUnknown() {
	xxx_messageInfo_ApiKey.DiscardUnknown(m)
}

var xxx_messageInfo_ApiKey proto.InternalMessageInfo

func (m *ApiKey) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *ApiKey) GetClientId() string {
	if m != nil {
		return m.ClientId
	}
	return ""
}

func (m *ApiKey) GetClientSecret() string {
	if m != nil {
		return m.ClientSecret
	}
	return ""
}

func (m *ApiKey) GetApplication() string {
	if m != nil {
		return m.Application
	}
	return ""
}

func (m *ApiKey) GetApplicationVersion() string {
	if m != nil {
		return m.ApplicationVersion
	}
	return ""
}

func (m *ApiKey) GetCompany() string {
	if m != nil {
		return m.Company
	}
	return ""
}

func (m *ApiKey) GetDateCreated() *timestamp.Timestamp {
	if m != nil {
		return m.DateCreated
	}
	return nil
}

// Request data to create new apiKey
type CreateRequest struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	// ApiKey entity to add
	ApiKey               *ApiKey  `protobuf:"bytes,2,opt,name=apiKey,proto3" json:"apiKey,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateRequest) Reset()         { *m = CreateRequest{} }
func (m *CreateRequest) String() string { return proto.CompactTextString(m) }
func (*CreateRequest) ProtoMessage()    {}
func (*CreateRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{1}
}

func (m *CreateRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateRequest.Unmarshal(m, b)
}
func (m *CreateRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateRequest.Marshal(b, m, deterministic)
}
func (m *CreateRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateRequest.Merge(m, src)
}
func (m *CreateRequest) XXX_Size() int {
	return xxx_messageInfo_CreateRequest.Size(m)
}
func (m *CreateRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateRequest proto.InternalMessageInfo

func (m *CreateRequest) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *CreateRequest) GetApiKey() *ApiKey {
	if m != nil {
		return m.ApiKey
	}
	return nil
}

// Response that contains data for created apiKey
type CreateResponse struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	// ID of created ApiKey
	Id int64 `protobuf:"varint,2,opt,name=id,proto3" json:"id,omitempty"`
	//
	ApiKey               *ApiKey  `protobuf:"bytes,3,opt,name=apiKey,proto3" json:"apiKey,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateResponse) Reset()         { *m = CreateResponse{} }
func (m *CreateResponse) String() string { return proto.CompactTextString(m) }
func (*CreateResponse) ProtoMessage()    {}
func (*CreateResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{2}
}

func (m *CreateResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateResponse.Unmarshal(m, b)
}
func (m *CreateResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateResponse.Marshal(b, m, deterministic)
}
func (m *CreateResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateResponse.Merge(m, src)
}
func (m *CreateResponse) XXX_Size() int {
	return xxx_messageInfo_CreateResponse.Size(m)
}
func (m *CreateResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateResponse.DiscardUnknown(m)
}

var xxx_messageInfo_CreateResponse proto.InternalMessageInfo

func (m *CreateResponse) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *CreateResponse) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *CreateResponse) GetApiKey() *ApiKey {
	if m != nil {
		return m.ApiKey
	}
	return nil
}

// Request data to read apiKey
type ReadRequest struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	// Unique integer identifier of the apiKey
	Id                   int64    `protobuf:"varint,2,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReadRequest) Reset()         { *m = ReadRequest{} }
func (m *ReadRequest) String() string { return proto.CompactTextString(m) }
func (*ReadRequest) ProtoMessage()    {}
func (*ReadRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{3}
}

func (m *ReadRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ReadRequest.Unmarshal(m, b)
}
func (m *ReadRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ReadRequest.Marshal(b, m, deterministic)
}
func (m *ReadRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReadRequest.Merge(m, src)
}
func (m *ReadRequest) XXX_Size() int {
	return xxx_messageInfo_ReadRequest.Size(m)
}
func (m *ReadRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ReadRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ReadRequest proto.InternalMessageInfo

func (m *ReadRequest) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *ReadRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

// Contains apiKey data specified in by ID request
type ReadResponse struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	// ApiKey entity read by ID
	ApiKey               *ApiKey  `protobuf:"bytes,2,opt,name=apiKey,proto3" json:"apiKey,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReadResponse) Reset()         { *m = ReadResponse{} }
func (m *ReadResponse) String() string { return proto.CompactTextString(m) }
func (*ReadResponse) ProtoMessage()    {}
func (*ReadResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{4}
}

func (m *ReadResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ReadResponse.Unmarshal(m, b)
}
func (m *ReadResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ReadResponse.Marshal(b, m, deterministic)
}
func (m *ReadResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReadResponse.Merge(m, src)
}
func (m *ReadResponse) XXX_Size() int {
	return xxx_messageInfo_ReadResponse.Size(m)
}
func (m *ReadResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ReadResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ReadResponse proto.InternalMessageInfo

func (m *ReadResponse) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *ReadResponse) GetApiKey() *ApiKey {
	if m != nil {
		return m.ApiKey
	}
	return nil
}

// Request data to update apiKey
type UpdateRequest struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	// ApiKey entity to update
	ApiKey               *ApiKey  `protobuf:"bytes,2,opt,name=apiKey,proto3" json:"apiKey,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpdateRequest) Reset()         { *m = UpdateRequest{} }
func (m *UpdateRequest) String() string { return proto.CompactTextString(m) }
func (*UpdateRequest) ProtoMessage()    {}
func (*UpdateRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{5}
}

func (m *UpdateRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateRequest.Unmarshal(m, b)
}
func (m *UpdateRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateRequest.Marshal(b, m, deterministic)
}
func (m *UpdateRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateRequest.Merge(m, src)
}
func (m *UpdateRequest) XXX_Size() int {
	return xxx_messageInfo_UpdateRequest.Size(m)
}
func (m *UpdateRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateRequest proto.InternalMessageInfo

func (m *UpdateRequest) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *UpdateRequest) GetApiKey() *ApiKey {
	if m != nil {
		return m.ApiKey
	}
	return nil
}

// Contains status of update operation
type UpdateResponse struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	// Contains number of entities have beed updated
	// Equals 1 in case of succesfull update
	Updated              int64    `protobuf:"varint,2,opt,name=updated,proto3" json:"updated,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UpdateResponse) Reset()         { *m = UpdateResponse{} }
func (m *UpdateResponse) String() string { return proto.CompactTextString(m) }
func (*UpdateResponse) ProtoMessage()    {}
func (*UpdateResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{6}
}

func (m *UpdateResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateResponse.Unmarshal(m, b)
}
func (m *UpdateResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateResponse.Marshal(b, m, deterministic)
}
func (m *UpdateResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateResponse.Merge(m, src)
}
func (m *UpdateResponse) XXX_Size() int {
	return xxx_messageInfo_UpdateResponse.Size(m)
}
func (m *UpdateResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateResponse proto.InternalMessageInfo

func (m *UpdateResponse) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *UpdateResponse) GetUpdated() int64 {
	if m != nil {
		return m.Updated
	}
	return 0
}

// Request data to delete ApiKey task
type DeleteRequest struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	//
	Company string `protobuf:"bytes,2,opt,name=company,proto3" json:"company,omitempty"`
	// Unique integer identifier of the ApiKey task to delete
	Id                   int64    `protobuf:"varint,3,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteRequest) Reset()         { *m = DeleteRequest{} }
func (m *DeleteRequest) String() string { return proto.CompactTextString(m) }
func (*DeleteRequest) ProtoMessage()    {}
func (*DeleteRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{7}
}

func (m *DeleteRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteRequest.Unmarshal(m, b)
}
func (m *DeleteRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteRequest.Marshal(b, m, deterministic)
}
func (m *DeleteRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteRequest.Merge(m, src)
}
func (m *DeleteRequest) XXX_Size() int {
	return xxx_messageInfo_DeleteRequest.Size(m)
}
func (m *DeleteRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteRequest proto.InternalMessageInfo

func (m *DeleteRequest) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *DeleteRequest) GetCompany() string {
	if m != nil {
		return m.Company
	}
	return ""
}

func (m *DeleteRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

// Contains status of delete operation
type DeleteResponse struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	// Contains number of entities have beed deleted
	// Equals 1 in case of succesfull delete
	Deleted              int64    `protobuf:"varint,2,opt,name=deleted,proto3" json:"deleted,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteResponse) Reset()         { *m = DeleteResponse{} }
func (m *DeleteResponse) String() string { return proto.CompactTextString(m) }
func (*DeleteResponse) ProtoMessage()    {}
func (*DeleteResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{8}
}

func (m *DeleteResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteResponse.Unmarshal(m, b)
}
func (m *DeleteResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteResponse.Marshal(b, m, deterministic)
}
func (m *DeleteResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteResponse.Merge(m, src)
}
func (m *DeleteResponse) XXX_Size() int {
	return xxx_messageInfo_DeleteResponse.Size(m)
}
func (m *DeleteResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteResponse.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteResponse proto.InternalMessageInfo

func (m *DeleteResponse) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *DeleteResponse) GetDeleted() int64 {
	if m != nil {
		return m.Deleted
	}
	return 0
}

// Request data to read all apiKeys
type ReadAllRequest struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion           string   `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ReadAllRequest) Reset()         { *m = ReadAllRequest{} }
func (m *ReadAllRequest) String() string { return proto.CompactTextString(m) }
func (*ReadAllRequest) ProtoMessage()    {}
func (*ReadAllRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{9}
}

func (m *ReadAllRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ReadAllRequest.Unmarshal(m, b)
}
func (m *ReadAllRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ReadAllRequest.Marshal(b, m, deterministic)
}
func (m *ReadAllRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReadAllRequest.Merge(m, src)
}
func (m *ReadAllRequest) XXX_Size() int {
	return xxx_messageInfo_ReadAllRequest.Size(m)
}
func (m *ReadAllRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ReadAllRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ReadAllRequest proto.InternalMessageInfo

func (m *ReadAllRequest) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

// Contains list of all apiKeys
type ReadAllResponse struct {
	// API versioning: it is my best practice to specify version explicitly
	ApiVersion string `protobuf:"bytes,1,opt,name=apiVersion,proto3" json:"apiVersion,omitempty"`
	// List of all apiKeys
	ApiKeys              []*ApiKey `protobuf:"bytes,2,rep,name=apiKeys,proto3" json:"apiKeys,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *ReadAllResponse) Reset()         { *m = ReadAllResponse{} }
func (m *ReadAllResponse) String() string { return proto.CompactTextString(m) }
func (*ReadAllResponse) ProtoMessage()    {}
func (*ReadAllResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_fb4da096f06f9281, []int{10}
}

func (m *ReadAllResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ReadAllResponse.Unmarshal(m, b)
}
func (m *ReadAllResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ReadAllResponse.Marshal(b, m, deterministic)
}
func (m *ReadAllResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ReadAllResponse.Merge(m, src)
}
func (m *ReadAllResponse) XXX_Size() int {
	return xxx_messageInfo_ReadAllResponse.Size(m)
}
func (m *ReadAllResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ReadAllResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ReadAllResponse proto.InternalMessageInfo

func (m *ReadAllResponse) GetApiVersion() string {
	if m != nil {
		return m.ApiVersion
	}
	return ""
}

func (m *ReadAllResponse) GetApiKeys() []*ApiKey {
	if m != nil {
		return m.ApiKeys
	}
	return nil
}

func init() {
	proto.RegisterType((*ApiKey)(nil), "v1.ApiKey")
	proto.RegisterType((*CreateRequest)(nil), "v1.CreateRequest")
	proto.RegisterType((*CreateResponse)(nil), "v1.CreateResponse")
	proto.RegisterType((*ReadRequest)(nil), "v1.ReadRequest")
	proto.RegisterType((*ReadResponse)(nil), "v1.ReadResponse")
	proto.RegisterType((*UpdateRequest)(nil), "v1.UpdateRequest")
	proto.RegisterType((*UpdateResponse)(nil), "v1.UpdateResponse")
	proto.RegisterType((*DeleteRequest)(nil), "v1.DeleteRequest")
	proto.RegisterType((*DeleteResponse)(nil), "v1.DeleteResponse")
	proto.RegisterType((*ReadAllRequest)(nil), "v1.ReadAllRequest")
	proto.RegisterType((*ReadAllResponse)(nil), "v1.ReadAllResponse")
}

func init() { proto.RegisterFile("apikey-service.proto", fileDescriptor_fb4da096f06f9281) }

var fileDescriptor_fb4da096f06f9281 = []byte{
	// 765 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xac, 0x55, 0x41, 0x73, 0xdb, 0x44,
	0x14, 0x1e, 0xc9, 0xa9, 0x4d, 0x9f, 0x63, 0xb7, 0x2c, 0x1c, 0x34, 0x86, 0xa1, 0x42, 0x74, 0xa6,
	0x69, 0x06, 0x6b, 0x13, 0x93, 0x53, 0x80, 0x19, 0x52, 0x7a, 0xa1, 0xdc, 0x94, 0x02, 0xc3, 0x70,
	0x80, 0x8d, 0xf4, 0x90, 0xb7, 0x95, 0xb4, 0xcb, 0xee, 0x2a, 0x25, 0xc3, 0x70, 0x61, 0x38, 0xc0,
	0x4c, 0x4f, 0x70, 0xe3, 0xc6, 0x6f, 0xe2, 0x2f, 0xf0, 0x43, 0x18, 0xed, 0x4a, 0x8e, 0x9c, 0x42,
	0xd0, 0xa1, 0x27, 0x7b, 0xbf, 0xf7, 0xbd, 0xf7, 0xbe, 0xf7, 0xed, 0xf3, 0x1a, 0x5e, 0x67, 0x92,
	0x3f, 0xc5, 0x8b, 0xa5, 0x46, 0x75, 0xce, 0x53, 0x8c, 0xa5, 0x12, 0x46, 0x10, 0xff, 0xfc, 0x70,
	0x71, 0x27, 0x17, 0x22, 0x2f, 0x90, 0x5a, 0xe4, 0xac, 0xfe, 0x96, 0x1a, 0x5e, 0xa2, 0x36, 0xac,
	0x94, 0x8e, 0xb4, 0x78, 0xb3, 0x25, 0x30, 0xc9, 0x29, 0xab, 0x2a, 0x61, 0x98, 0xe1, 0xa2, 0xd2,
	0x6d, 0xf4, 0x5d, 0xfb, 0x91, 0x2e, 0x73, 0xac, 0x96, 0xfa, 0x19, 0xcb, 0x73, 0x54, 0x54, 0x48,
	0xcb, 0x78, 0x91, 0x1d, 0xfd, 0xec, 0xc3, 0xf8, 0x44, 0xf2, 0x4f, 0xf1, 0x82, 0xcc, 0xc1, 0xe7,
	0x59, 0xe0, 0x85, 0xde, 0xde, 0x28, 0xf1, 0x79, 0x46, 0xde, 0x80, 0x9b, 0x69, 0xc1, 0xb1, 0x32,
	0x5f, 0xf3, 0x2c, 0xf0, 0x43, 0x6f, 0xef, 0x66, 0xf2, 0x8a, 0x03, 0x3e, 0xc9, 0xc8, 0x3b, 0x30,
	0x6b, 0x83, 0x1a, 0x53, 0x85, 0x26, 0x18, 0x59, 0xc2, 0xae, 0x03, 0x4f, 0x2d, 0x46, 0x42, 0x98,
	0x32, 0x29, 0x0b, 0x9e, 0xda, 0x96, 0xc1, 0x8e, 0xa5, 0xf4, 0x21, 0x12, 0x03, 0xe9, 0x1d, 0x3f,
	0x47, 0xa5, 0x1b, 0xe2, 0x0d, 0x4b, 0xfc, 0x97, 0x08, 0x09, 0x60, 0x92, 0x8a, 0x52, 0xb2, 0xea,
	0x22, 0x18, 0x5b, 0x52, 0x77, 0x24, 0x1f, 0xc0, 0x34, 0x63, 0x06, 0x3f, 0x56, 0xc8, 0x0c, 0x66,
	0xc1, 0x24, 0xf4, 0xf6, 0xa6, 0xab, 0x45, 0xec, 0xac, 0x8a, 0x3b, 0x2f, 0xe3, 0xc7, 0x9d, 0x97,
	0x49, 0x9f, 0x1e, 0x9d, 0xc2, 0xcc, 0x7d, 0x4d, 0xf0, 0xbb, 0x1a, 0xb5, 0x21, 0x6f, 0x01, 0x30,
	0xc9, 0x3b, 0x41, 0x9e, 0xed, 0xd5, 0x43, 0x48, 0x04, 0x63, 0x66, 0x6d, 0xb3, 0xce, 0x4c, 0x57,
	0x10, 0x9f, 0x1f, 0xc6, 0xce, 0xc8, 0xa4, 0x8d, 0x44, 0x19, 0xcc, 0xbb, 0xa2, 0x5a, 0x8a, 0x4a,
	0xe3, 0xff, 0x56, 0x75, 0x57, 0xe0, 0x6f, 0xae, 0xe0, 0xb2, 0xcb, 0xe8, 0x3f, 0xbb, 0x7c, 0x08,
	0xd3, 0x04, 0x59, 0x36, 0x54, 0xf8, 0x95, 0x16, 0x51, 0x02, 0xbb, 0x2e, 0x7d, 0xa0, 0xc4, 0x21,
	0x83, 0x9f, 0xc2, 0xec, 0x33, 0x99, 0xbd, 0x64, 0x37, 0x1f, 0xc1, 0xbc, 0x2b, 0x3a, 0x50, 0x6a,
	0x00, 0x93, 0xda, 0x66, 0x74, 0xf3, 0x76, 0xc7, 0xe8, 0x4b, 0x98, 0x3d, 0xc4, 0x02, 0x87, 0x0b,
	0xec, 0xed, 0x9d, 0xbf, 0xbd, 0x77, 0xce, 0xcf, 0xd1, 0xc6, 0xcf, 0x47, 0x30, 0xef, 0x4a, 0x0f,
	0x97, 0x99, 0xd9, 0x8c, 0x8d, 0xcc, 0xf6, 0x18, 0x1d, 0xc0, 0xbc, 0xb9, 0x9b, 0x93, 0xa2, 0x18,
	0xa8, 0x33, 0xfa, 0x02, 0x6e, 0x6d, 0x32, 0x06, 0xb6, 0xbf, 0x0b, 0x13, 0xe7, 0xb0, 0x0e, 0xfc,
	0x70, 0x74, 0xc5, 0xfc, 0x2e, 0xb4, 0x7a, 0x3e, 0x82, 0x99, 0xc3, 0x4e, 0xdd, 0x83, 0x45, 0xbe,
	0x81, 0xb1, 0xdb, 0x6e, 0xf2, 0x6a, 0x93, 0xb0, 0xf5, 0xf3, 0x59, 0x90, 0x3e, 0xe4, 0x84, 0x44,
	0xf4, 0xa7, 0xbf, 0xfe, 0xfe, 0xdd, 0xbf, 0x1f, 0xdd, 0xa5, 0x52, 0x28, 0x53, 0x57, 0xb5, 0xa6,
	0x87, 0xf1, 0x41, 0x7c, 0x40, 0x59, 0x6d, 0xd6, 0xd4, 0x3d, 0x87, 0x34, 0xc7, 0x0a, 0x15, 0x33,
	0x78, 0xec, 0xed, 0x93, 0xfb, 0xb0, 0xd3, 0x0c, 0x43, 0x6e, 0x35, 0xc5, 0x7a, 0x3b, 0xbe, 0xb8,
	0x7d, 0x09, 0xb4, 0x43, 0x52, 0x18, 0xbb, 0xe5, 0x70, 0x62, 0xb6, 0xb6, 0xcf, 0x89, 0xb9, 0xb2,
	0x3b, 0x5f, 0xc1, 0xa4, 0x35, 0x8a, 0x90, 0xae, 0xda, 0xa5, 0xcf, 0x8b, 0xd7, 0xb6, 0xb0, 0x76,
	0x80, 0x7b, 0x76, 0x80, 0xb7, 0xc9, 0x9d, 0xeb, 0x06, 0x60, 0x45, 0xd1, 0x58, 0xe3, 0x76, 0xc0,
	0xa9, 0xd9, 0x5a, 0x35, 0xa7, 0x66, 0x7b, 0x45, 0x3a, 0x6b, 0xf6, 0xef, 0x5d, 0x57, 0x59, 0x61,
	0x29, 0xce, 0x91, 0xfe, 0xc0, 0xb3, 0x1f, 0x1f, 0x3c, 0xf7, 0x7f, 0x3b, 0xf9, 0xc5, 0x27, 0x7f,
	0x7a, 0xb0, 0xfb, 0x58, 0x3c, 0x14, 0x61, 0xfb, 0x2f, 0x12, 0xfd, 0xea, 0x01, 0xcd, 0xc5, 0x32,
	0x57, 0x32, 0x5d, 0xae, 0x8d, 0x91, 0x4b, 0x85, 0xda, 0x2c, 0x4b, 0x9e, 0x2a, 0xd1, 0x52, 0x96,
	0xa6, 0x36, 0x42, 0x71, 0x56, 0x84, 0x52, 0x89, 0x27, 0x98, 0x1a, 0xf2, 0xa0, 0x21, 0xea, 0x63,
	0x4a, 0x73, 0x6e, 0xd6, 0xf5, 0x59, 0x9c, 0x8a, 0x92, 0xb2, 0x52, 0x8b, 0xa7, 0xa2, 0x18, 0x5a,
	0x6b, 0x41, 0x4a, 0xcc, 0x78, 0x5d, 0x7e, 0xd4, 0xe6, 0x35, 0x35, 0x56, 0x37, 0xec, 0x08, 0xfb,
	0x9e, 0xb7, 0xba, 0xdd, 0x7b, 0xc9, 0xe9, 0x13, 0x2d, 0xaa, 0xe3, 0x17, 0x90, 0xe4, 0x7d, 0x18,
	0x1d, 0x1d, 0x1c, 0x91, 0x23, 0xd8, 0x4f, 0xd0, 0xd4, 0xaa, 0xc2, 0x2c, 0x7c, 0xb6, 0xc6, 0x2a,
	0x34, 0x6b, 0x0c, 0x15, 0x6a, 0x51, 0xab, 0x14, 0xc3, 0x4c, 0xa0, 0x0e, 0x2b, 0x61, 0x42, 0xfc,
	0x9e, 0x6b, 0x13, 0x93, 0x31, 0xec, 0xfc, 0xe1, 0x7b, 0x93, 0xb3, 0xb1, 0x7d, 0xdf, 0xdf, 0xfb,
	0x27, 0x00, 0x00, 0xff, 0xff, 0x83, 0xa4, 0xea, 0xec, 0x55, 0x07, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// ApiKeyServiceClient is the client API for ApiKeyService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ApiKeyServiceClient interface {
	// Create new ApiKey task
	Create(ctx context.Context, in *CreateRequest, opts ...grpc.CallOption) (*CreateResponse, error)
	// Read apiKey
	Read(ctx context.Context, in *ReadRequest, opts ...grpc.CallOption) (*ReadResponse, error)
	// Update apiKey
	Update(ctx context.Context, in *UpdateRequest, opts ...grpc.CallOption) (*UpdateResponse, error)
	// Read all apiKeys
	ReadAll(ctx context.Context, in *ReadAllRequest, opts ...grpc.CallOption) (*ReadAllResponse, error)
	// Delete ApiKey task
	Delete(ctx context.Context, in *DeleteRequest, opts ...grpc.CallOption) (*DeleteResponse, error)
}

type apiKeyServiceClient struct {
	cc *grpc.ClientConn
}

func NewApiKeyServiceClient(cc *grpc.ClientConn) ApiKeyServiceClient {
	return &apiKeyServiceClient{cc}
}

func (c *apiKeyServiceClient) Create(ctx context.Context, in *CreateRequest, opts ...grpc.CallOption) (*CreateResponse, error) {
	out := new(CreateResponse)
	err := c.cc.Invoke(ctx, "/v1.ApiKeyService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apiKeyServiceClient) Read(ctx context.Context, in *ReadRequest, opts ...grpc.CallOption) (*ReadResponse, error) {
	out := new(ReadResponse)
	err := c.cc.Invoke(ctx, "/v1.ApiKeyService/Read", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apiKeyServiceClient) Update(ctx context.Context, in *UpdateRequest, opts ...grpc.CallOption) (*UpdateResponse, error) {
	out := new(UpdateResponse)
	err := c.cc.Invoke(ctx, "/v1.ApiKeyService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apiKeyServiceClient) ReadAll(ctx context.Context, in *ReadAllRequest, opts ...grpc.CallOption) (*ReadAllResponse, error) {
	out := new(ReadAllResponse)
	err := c.cc.Invoke(ctx, "/v1.ApiKeyService/ReadAll", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *apiKeyServiceClient) Delete(ctx context.Context, in *DeleteRequest, opts ...grpc.CallOption) (*DeleteResponse, error) {
	out := new(DeleteResponse)
	err := c.cc.Invoke(ctx, "/v1.ApiKeyService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ApiKeyServiceServer is the server API for ApiKeyService service.
type ApiKeyServiceServer interface {
	// Create new ApiKey task
	Create(context.Context, *CreateRequest) (*CreateResponse, error)
	// Read apiKey
	Read(context.Context, *ReadRequest) (*ReadResponse, error)
	// Update apiKey
	Update(context.Context, *UpdateRequest) (*UpdateResponse, error)
	// Read all apiKeys
	ReadAll(context.Context, *ReadAllRequest) (*ReadAllResponse, error)
	// Delete ApiKey task
	Delete(context.Context, *DeleteRequest) (*DeleteResponse, error)
}

// UnimplementedApiKeyServiceServer can be embedded to have forward compatible implementations.
type UnimplementedApiKeyServiceServer struct {
}

func (*UnimplementedApiKeyServiceServer) Create(ctx context.Context, req *CreateRequest) (*CreateResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (*UnimplementedApiKeyServiceServer) Read(ctx context.Context, req *ReadRequest) (*ReadResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Read not implemented")
}
func (*UnimplementedApiKeyServiceServer) Update(ctx context.Context, req *UpdateRequest) (*UpdateResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (*UnimplementedApiKeyServiceServer) ReadAll(ctx context.Context, req *ReadAllRequest) (*ReadAllResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ReadAll not implemented")
}
func (*UnimplementedApiKeyServiceServer) Delete(ctx context.Context, req *DeleteRequest) (*DeleteResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}

func RegisterApiKeyServiceServer(s *grpc.Server, srv ApiKeyServiceServer) {
	s.RegisterService(&_ApiKeyService_serviceDesc, srv)
}

func _ApiKeyService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApiKeyServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.ApiKeyService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApiKeyServiceServer).Create(ctx, req.(*CreateRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApiKeyService_Read_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApiKeyServiceServer).Read(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.ApiKeyService/Read",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApiKeyServiceServer).Read(ctx, req.(*ReadRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApiKeyService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApiKeyServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.ApiKeyService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApiKeyServiceServer).Update(ctx, req.(*UpdateRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApiKeyService_ReadAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ReadAllRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApiKeyServiceServer).ReadAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.ApiKeyService/ReadAll",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApiKeyServiceServer).ReadAll(ctx, req.(*ReadAllRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _ApiKeyService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ApiKeyServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/v1.ApiKeyService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ApiKeyServiceServer).Delete(ctx, req.(*DeleteRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ApiKeyService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "v1.ApiKeyService",
	HandlerType: (*ApiKeyServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _ApiKeyService_Create_Handler,
		},
		{
			MethodName: "Read",
			Handler:    _ApiKeyService_Read_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _ApiKeyService_Update_Handler,
		},
		{
			MethodName: "ReadAll",
			Handler:    _ApiKeyService_ReadAll_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _ApiKeyService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "apikey-service.proto",
}
