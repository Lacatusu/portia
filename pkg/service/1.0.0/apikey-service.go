////////////////////////////////////////////////////////////////////////////////////////////
///// I NEED TO MIGRATE MYSQL TO CASSANDRA DATABASE  /////////////////////////////////////////
////

package v1

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"fmt"
	"time"

	v1 "github.com/flacatusu/portunus/pkg/api/1.0.0"

	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	// apiVersion is version of API is provided by server
	apiVersion = "1.0.0"
)

// ApiKeyServiceServer is implementation of v1.ApiKeyServiceServer proto interface
type apiKeyServiceServer struct {
	db *sql.DB
}

//ApiKeyServiceServer creates ApiKey service
func ApiKeyServiceServer(db *sql.DB) v1.ApiKeyServiceServer {
	return &apiKeyServiceServer{db: db}
}

// checkAPI checks if the API version requested by client is supported by server
func (s *apiKeyServiceServer) checkAPI(api string) error {
	// API version is "" means use current version of the service
	if len(api) > 0 {
		if apiVersion != api {
			return status.Errorf(codes.Unimplemented,
				"unsupported API version: service implements API version '%s', but asked for '%s'", apiVersion, api)
		}
	}
	return nil
}

// connect returns SQL database connection from the pool
func (s *apiKeyServiceServer) connect(ctx context.Context) (*sql.Conn, error) {
	c, err := s.db.Conn(ctx)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to connect to database-> "+err.Error())
	}
	return c, nil
}

func signKey(someid string, secret []byte) string {
	mac := hmac.New(sha256.New, secret)
	mac.Write([]byte(someid))
	b := mac.Sum(nil)
	return hex.EncodeToString(b)
}

func validateSignature(id, signature string, secret []byte) bool {
	mac := hmac.New(sha256.New, secret)
	mac.Write([]byte(id))
	expectedMAC := mac.Sum(nil)
	signatureMAC, err := hex.DecodeString(signature)
	if err != nil {
		fmt.Println("PROBLEM IN DECODING HUH!")
		return false
	}
	return hmac.Equal(expectedMAC, signatureMAC)
}

// Create new ApiKey apikey
func (s *apiKeyServiceServer) Create(ctx context.Context, req *v1.CreateRequest) (*v1.CreateResponse, error) {
	data := uuid.New().String()

	apiKeySecret := []byte(uuid.New().String())

	sign := signKey(data, apiKeySecret)

	fmt.Println(sign)
	valid := validateSignature(data, sign, apiKeySecret)

	fmt.Println(valid)

	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.ApiVersion); err != nil {
		return nil, err
	}
	apiKey := sign

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()
	dateCreated := time.Now()

	// insert apikey entity data
	res, err := c.ExecContext(ctx, "INSERT INTO ApiKey(`Client_id`, `Client_secret`, `Application`, `ApplicationVersion`, `Company`, `DateCreated`) VALUES(?, ?, ?, ?,?, ?)",
		apiKey, apiKeySecret, req.ApiKey.Application, req.ApiKey.ApplicationVersion, req.ApiKey.Company, dateCreated)
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "failed to insert into ApiKey-> "+err.Error())
	}

	// get ID of creates apiKey
	id, err := res.LastInsertId()
	td := req.ApiKey
	td.ClientId = sign
	td.ClientSecret = string(apiKeySecret)
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "failed to retrieve id for created apikey-> "+err.Error())
	}

	return &v1.CreateResponse{
		ApiVersion: apiVersion,
		Id:         id,
		ApiKey:     td,
	}, nil
}

// Read ApiKey apikey
func (s *apiKeyServiceServer) Read(ctx context.Context, req *v1.ReadRequest) (*v1.ReadResponse, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.ApiVersion); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	// query ApiKey by ID
	rows, err := c.QueryContext(ctx, "SELECT `ID`, `Client_id`, `Client_secret`, `Application`, `Company`, `DateCreated` FROM Credentials WHERE `ID`=?",
		req.Id)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to select from ApiKey-> "+err.Error())
	}
	defer rows.Close()

	if !rows.Next() {
		if err := rows.Err(); err != nil {
			return nil, status.Error(codes.Unknown, "failed to retrieve data from ApiKey-> "+err.Error())
		}
		return nil, status.Error(codes.NotFound, fmt.Sprintf("ApiKey with ID='%d' is not found",
			req.Id))
	}

	// get ApiKey data
	var td v1.ApiKey
	var reminder time.Time
	if err := rows.Scan(&td.Id, &td.ClientId); err != nil {
		return nil, status.Error(codes.Unknown, "failed to retrieve field values from ApiKey row-> "+err.Error())
	}
	td.DateCreated, err = ptypes.TimestampProto(reminder)
	if err != nil {
		return nil, status.Error(codes.Unknown, "reminder field has invalid format-> "+err.Error())
	}

	if rows.Next() {
		return nil, status.Error(codes.Unknown, fmt.Sprintf("found multiple ApiKey rows with ID='%d'",
			req.Id))
	}

	return &v1.ReadResponse{
		ApiVersion: apiVersion,
		ApiKey:     &td,
	}, nil

}

// Update ApiKey apikey
func (s *apiKeyServiceServer) Update(ctx context.Context, req *v1.UpdateRequest) (*v1.UpdateResponse, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.ApiVersion); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	reminder, err := ptypes.Timestamp(req.ApiKey.DateCreated)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "reminder field has invalid format-> "+err.Error())
	}

	// update ApiKey
	res, err := c.ExecContext(ctx, "UPDATE ApiKey SET `Title`=?, `Description`=?, `Reminder`=? WHERE `ID`=?",
		req.ApiKey.ClientId, req.ApiKey.ClientSecret, reminder, req.ApiKey.Id)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to update ApiKey-> "+err.Error())
	}

	rows, err := res.RowsAffected()
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to retrieve rows affected value-> "+err.Error())
	}

	if rows == 0 {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("ApiKey with ID='%d' is not found",
			req.ApiKey.Id))
	}

	return &v1.UpdateResponse{
		ApiVersion: apiVersion,
		Updated:    rows,
	}, nil
}

// Delete ApiKey apikey
func (s *apiKeyServiceServer) Delete(ctx context.Context, req *v1.DeleteRequest) (*v1.DeleteResponse, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.ApiVersion); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	// delete ApiKey
	res, err := c.ExecContext(ctx, "DELETE FROM ApiKey WHERE `ID`=?", req.Id)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to delete ApiKey-> "+err.Error())
	}

	rows, err := res.RowsAffected()
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to retrieve rows affected value-> "+err.Error())
	}

	if rows == 0 {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("ApiKey with ID='%d' is not found",
			req.Id))
	}

	return &v1.DeleteResponse{
		ApiVersion: apiVersion,
		Deleted:    rows,
	}, nil
}

// Read all ApiKey apikeys
func (s *apiKeyServiceServer) ReadAll(ctx context.Context, req *v1.ReadAllRequest) (*v1.ReadAllResponse, error) {
	// check if the API version requested by client is supported by server
	if err := s.checkAPI(req.ApiVersion); err != nil {
		return nil, err
	}

	// get SQL connection from pool
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	// get ApiKey list
	rows, err := c.QueryContext(ctx, "SELECT * FROM ApiKey")
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to select from ApiKey-> "+err.Error())
	}
	defer rows.Close()

	var reminder time.Time
	list := []*v1.ApiKey{}
	for rows.Next() {
		td := new(v1.ApiKey)
		if err := rows.Scan(&td.Id, &td.ClientId, &td.ClientSecret, &td.Application, &td.ApplicationVersion, &td.Company, &reminder); err != nil {
			return nil, status.Error(codes.PermissionDenied, "failed to retrieve field values from ApiKey row-> "+err.Error())
		}
		td.DateCreated, err = ptypes.TimestampProto(reminder)
		if err != nil {
			return nil, status.Error(codes.PermissionDenied, "reminder field has invalid format-> "+err.Error())
		}
		list = append(list, td)
	}

	if err := rows.Err(); err != nil {
		return nil, status.Error(codes.PermissionDenied, "failed to retrieve data from ApiKey-> "+err.Error())
	}
	return &v1.ReadAllResponse{
		ApiVersion: apiVersion,
		ApiKeys:    list,
	}, nil
}
